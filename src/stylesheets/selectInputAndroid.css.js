import {
  StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  defaultcontainerstyle: {
    flexDirection:              'column',
    justifyContent:             'center',
  },
  defaultlabelstyle: {
    alignSelf:                  'center',
    fontSize:                   13,
  },
  modal: {
    backgroundColor: 'rgb(255,255,255,0.5)'
  }
});

export default styles;
